#!/bin/sh

cat << EOF > .gitattributes
/**/vault.yml diff=ansible-vault merge=binary
EOF

cat << EOF >> .git/config
[diff "ansible-vault"]
    textconv = ansible-vault view
EOF
