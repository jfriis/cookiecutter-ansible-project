=================================================
{{cookiecutter.gitlab_project}}
=================================================

{{cookiecutter.project_short_description}}

https://gitlab.com/{{cookiecutter.gitlab_namespace}}/{{cookiecutter.gitlab_project}}


.. _usage:

Usage
=====

.. code::

   make install
   make roles
   make vault
   . virt/bin/activate
   vagrant up
   vagrant provision
   ansible-playbook playbook.yml
