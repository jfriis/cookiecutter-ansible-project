import os
from pathlib import Path
import pytest
import tempfile
import subprocess
import shutil

PROJECT = 'gitlab-project-name'


@pytest.fixture(scope="module")
def cookiecutter_output():
    """
    Use cookiecutter on this project and check that the directory appears.
    """
    tmpdir = tempfile.mkdtemp()
    cmd = 'cookiecutter --config-file tests/cookiecutter.yml --no-input --output-dir tmpdir -f .'
    subprocess.check_output(cmd, shell=True)
    yield str(Path(tmpdir) / PROJECT)
    shutil.rmtree(tmpdir)


def test_cookiecutter(cookiecutter_output):
    assert os.path.isdir(cookiecutter_output)
