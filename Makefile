VIRT ?= virt


.PHONY: install

install $(VIRT):
	python3 -m venv $(VIRT)
	$(VIRT)/bin/pip install --upgrade pip setuptools wheel
	$(VIRT)/bin/pip install -r requirements.txt


.PHONY: test

test: $(VIRT)
	$(VIRT)/bin/pytest tests/


.PHONY: html

html: $(VIRT)
	@cd docs && $(MAKE) $@
