============================
cookiecutter-ansible-project
============================

Cookiecutter for an Ansible project

https://gitlab.com/jfriis/cookiecutter-ansible-project


Usage
=====

.. code:: shell

   cruft create https://gitlab.com/jfriis/cookiecutter-ansible-project.git


Features
========

- Sensible structure
- Requirements
- Ansible Vault
- Vagrant


.. toctree::
   :maxdepth: 2
   :caption: Contents

   usage
   configuration
   changelog
